from django.test import TestCase
from .models import *
from bs4 import BeautifulSoup

class TestViewLibrary(TestCase):
    fixtures = ['init']
    
    def test_nb_library(self):
        self.assertEqual(Library.objects.count(), 2)
        
    def test_nb_li(self):
        response = self.client.get('/library/')
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(len(soup.find_all('li')), 2)
        
class TestViewUserLibrary(TestCase):
    fixtures = ['init']
    
    def test_nb_li_cyliann(self):
        response = self.client.get('/library/1/')
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(len(soup.find_all('li')), 2)

    def test_nb_li_mael(self):
        response = self.client.get('/library/2/')
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(len(soup.find_all('li')), 1)

        
class TestViewGameInfo(TestCase):
    fixtures = ['init']
    
    def test_tags(self):
        response = self.client.get('/games/2/')
        soup = BeautifulSoup(response.content, 'html.parser')
        tags = [tag.text for tag in soup.find_all('li', {'class': 'tag'})]
        self.assertIn('aventure', tags)
        self.assertIn('plateforme', tags)
        
    def test_price(self):
        response = self.client.get('/games/2/')
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(soup.find('li', {'class': 'price'}).text, 'Prix: 120.00€')