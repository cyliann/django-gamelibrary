from django.shortcuts import render
from django.http import HttpResponse, HttpResponseServerError
from .models import *
from django.template import loader

# Create your views here.

def index(request):
    return HttpResponse("oui")

def library(request):
    library = Library.objects.all()
    template = loader.get_template('library.html')
    context = {'libraries': library}
    return HttpResponse(template.render(context, request))

def games(request, library_id):
        library = Library.objects.get(id = library_id)
        context = {
            'library': library,
            'games': library.game_set.all(),
        }
        return render(request, 'games.html', context)

def details(request, game_id):
        game = Game.objects.get(id = game_id)
        context = {
            'game': game,
            'img': game.img,
            'price': game.price,
            'tags': game.tag_set.all(),
        }
        return render(request, 'details.html', context)