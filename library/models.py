from django.db import models 

class Library(models.Model):
    name = models.CharField(max_length=100)
    
    def __str__(self):
        return self.name
    
class Game(models.Model):
    name = models.CharField(max_length=100)
    img = models.CharField(max_length=1000)
    price = models.DecimalField(max_digits=5, decimal_places=2)
    library = models.ManyToManyField(Library)
   
    def __str__(self):
        return self.name
    
class Tag(models.Model):
    name = models.CharField(max_length=100)
    game = models.ManyToManyField(Game)
    
    def __str__(self):
        return self.name