from django.test import TestCase
from .models import *
# Create your tests here.

class LibraryTest(TestCase):
    
    def test_string(self):
        lib = Library(name="Test Library")
        self.assertEqual(str(lib), lib.name)

    def test_insert_data(self):
        lib = Library(name="Test Library")
        lib.save()
        record = Library.objects.get(pk=lib.id)
        self.assertEqual(record, lib)
        
    def test_delete_data(self):
        lib = Library(name="Test Library")
        lib.save()
        record = Library.objects.get(pk=lib.id)
        record.delete()
        with self.assertRaises(Library.DoesNotExist):
            Library.objects.get(pk=lib.id)
            
class TagTest(TestCase):
    def test_string(self):
        tag = Tag(name="Test Tag")
        self.assertEqual(str(tag), tag.name)
        
    def test_insert_data(self):
        tag = Tag(name="Test Tag")
        tag.save()
        record = Tag.objects.get(pk=tag.id)
        self.assertEqual(record, tag)
        
    def test_delete_data(self):
        tag = Tag(name="Test Tag")
        tag.save()
        record = Tag.objects.get(pk=tag.id)
        record.delete()
        with self.assertRaises(Tag.DoesNotExist):
            Tag.objects.get(pk=tag.id)
            
    def test_associate_to_game(self):
        game = Game(name="Test Game", price = 10.00, img = "test.jpg")
        game.save()
        tag = Tag(name="Test Tag")
        tag.save()
        game.tag_set.add(tag)
        game.save()
        self.assertEqual(game.tag_set.all()[0].name, "Test Tag")
            
class GameTest(TestCase):
    def test_string(self):
        game = Game(name="Test Game")
        self.assertEqual(str(game), game.name)
        
    def test_insert_data(self):
        game = Game(name="Test Game", price = 10.00, img = "test.jpg")
        game.save()
        record = Game.objects.get(pk=game.id)
        self.assertEqual(record, game)
        
    def test_delete_data(self):
        game = Game(name="Test Game", price = 10.00, img = "test.jpg")
        game.save()
        record = Game.objects.get(pk=game.id)
        record.delete()
        with self.assertRaises(Game.DoesNotExist):
            Game.objects.get(pk=game.id)